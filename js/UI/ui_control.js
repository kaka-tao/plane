import UI_Frame from './ui_frame'
/* 此文件用来显示各种界面 */

const frame = new UI_Frame()

//枚举，定义当前ui的下标
const ui_fsm = {
  home  :Symbol('home_ui'),//主界面
  afk   :Symbol('afk_ui')//挂机界面
}

export default class UI{
  constructor(){
      this.fsm = ui_fsm.home//当前界面
      this.pre_fsm = ui_fsm.home//上一个界面
  }

  show_home_ui(ctx)
  {
    frame.drawToCanvas(ctx)
  }
  
  update(ctx){
    switch (this.fsm)
    {
      case ui_fsm.home:
        this.show_home_ui(ctx)
        break
    }
  }

  flash(ctx){
      if (this.fsm==this.pre_fsm)//两个界面一致 部分刷新
      {
        this.update(ctx)
      }
      else {//两个界面不一致 全刷新

      }
      this.pre_fsm = this.fsm//保存现场
  }
}