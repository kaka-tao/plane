import Sprite from '../base/sprite'
//此文件绘制头像框及相关属性
const Frame_SRC = 'images/ui_frame.png'
const Frame_WIDTH = 60
const Frame_HEIGHT = 60

export default class UI_Frame extends Sprite{
  constructor(){
    super(Frame_SRC, Frame_WIDTH, Frame_HEIGHT)
  }
}
